% Copyright 2018 Ahmadou
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License version 1.3c,
% available at http://www.latex-project.org/lppl/.

\documentclass{ltxdoc}

\usepackage[numbered]{hypdoc}
\usepackage{hologo}
\usepackage{hyperref,xcolor}
\usepackage{longtable,booktabs}
\usepackage{./humicons}

\definecolor{blue}{rgb}{0.19,0.31,0.54}
\hypersetup{colorlinks=true, linkcolor=blue, urlcolor=blue, hyperindex}

\usepackage{totcount}
\newtotcounter{IconsCounter}
\setcounter{IconsCounter}{0}

\EnableCrossrefs
\CodelineIndex
\RecordChanges

\changes{v2.0}{2018/12/25}{First version}

\begin{document}
\title{The \textsf{\jobname} package\\
High quality humanitarian icons}
\author{%
  Ahmadou Dicko\thanks{Email: \href{mailto:mail@ahmadoudicko.com}{\tt mail@ahmadoudicko.com}}~~(\hologo{LaTeX} code)\\%
  OCHA Advocacy and Visual Media Unit (font and icons design)}
\date{v2.0, released on December 2018}
\maketitle

\begin{abstract}
The \textsf{\jobname} package provides specific \hologo{(La)TeX} bindings with the free \emph{OCHA Humanitarian Icons v.02} font, allowing access to \total{IconsCounter} high quality humanitarian icons.
\end{abstract}

\bigskip

\section{Description}
The \textsf{\jobname} package provides access in \hologo{(La)TeX} to \total{IconsCounter} high quality humanitarian icons included in the free \emph{OCHA Humanitarian icon}. This package requires either the \hologo{Xe}\hologo{(La)TeX} or Lua\hologo{(La)TeX} engine to load the \emph{Humanitarian-Icons-v02} font from the system, which requires installing the bundled \texttt{Humanitarian-Icons-v02.ttf} font file.
As new releases come out, it is recommended to install the bundled font version as there may be differences between the package and previous font versions or newest font versions not yet contemplated in the package.

The \emph{OCHA Humanitarian Icons v.02} font was designed by OCHA Advocacy and Visual Media Unit and released under the xxx License. This package is a redistribution of the free \emph{OCHA Humanitarian Icons v.02} font with specific bindings for \hologo{(La)TeX}.
It is inspired and based on the \textsf{academicons}\footnote{Available at \url{http://www.ctan.org/pkg/academicons}.} and \textsf{fontawesome}\footnote{Available at \url{http://www.ctan.org/pkg/fontawesome}.} packages.

\section{Usage}
\DescribeMacro{\hiicon}
The \textsf{\jobname} package provides the generic \cs{hiicon} command to access icons, which takes as mandatory argument the \meta{name} of the desired icon. It also provides individual direct commands for each specific icon. The full list of icons and their respective names and direct commands can be found below. For example, \cs{hiicon\{FoodSecurity\}} yields the same result as \cs{hiFoodSecurity}.

\newenvironment{showcase}%
  {%
   \begin{longtable}{cp{3.75cm}p{4.5cm}}
   \cmidrule[\heavyrulewidth]{1-3}% \toprule
 %   \bfseries \#&\bfseries Icon&{\bfseries Name} (\meta{name})&\bfseries Direct Command\\
   \bfseries Icon&{\bfseries Name} (\meta{name})&\bfseries Direct Command\\
   \cmidrule{1-3}\endhead}
  {\cmidrule[\heavyrulewidth]{1-3}% \bottomrule
   \end{longtable}}
\newcommand{\icon}[2]{%
\stepcounter{IconsCounter}%
\hiicon{#1}&%
\itshape #1&%
\ttfamily\textbackslash #2\index{\ttfamily\textbackslash #2}\\%
}

\newpage
\subsection{Humanitarian Icons}
\label{sec:icons}

\begin{showcase}
\icon{abduction-kidnapping}{hiAbductionKidnapping}
\icon{about}{hiAbout}
\icon{add-document}{hiAddDocument}
\icon{add}{hiAdd}
\icon{advocacy}{hiAdvocacy}
\icon{affected-population}{hiAffectedPopulation}
\icon{agriculture}{hiAgriculture}
\icon{airport-affected}{hiAirportAffected}
\icon{airport-destroyed}{hiAirportDestroyed}
\icon{airport-military}{hiAirportMilitary}
\icon{airport-not-affected}{hiAirportNotAffected}
\icon{airport}{hiAirport}
\icon{alert}{hiAlert}
\icon{analysis}{hiAnalysis}
\icon{arrest-detention}{hiArrestDetention}
\icon{assault}{hiAssault}
\icon{assembly-point}{hiAssemblyPoint}
\icon{assessment}{hiAssessment}
\icon{attack}{hiAttack}
\icon{blanket}{hiBlanket}
\icon{blog}{hiBlog}
\icon{boat}{hiBoat}
\icon{bookmark}{hiBookmark}
\icon{border-crossing}{hiBorderCrossing}
\icon{borehole}{hiBorehole}
\icon{bottled-water}{hiBottledWater}
\icon{bridge-affected}{hiBridgeAffected}
\icon{bridge-destroyed}{hiBridgeDestroyed}
\icon{bridge-not-affected}{hiBridgeNotAffected}
\icon{bridge}{hiBridge}
\icon{bucket}{hiBucket}
\icon{buddhist-temple}{hiBuddhistTemple}
\icon{building-facility-affected}{hiBuildingFacilityAffected}
\icon{building-facility-destroyed}{hiBuildingFacilityDestroyed}
\icon{building-facility-not-affected}{hiBuildingFacilityNotAffected}
\icon{building}{hiBuilding}
\icon{bus}{hiBus}
\icon{calendar}{hiCalendar}
\icon{camp-coordination-and-camp-management}{hiCampCoordinationAndCampManagement}
\icon{car}{hiCar}
\icon{carjacking}{hiCarjacking}
\icon{cash-transfer}{hiCashTransfer}
\icon{cell-tower}{hiCellTower}
\icon{chart}{hiChart}
\icon{chat}{hiChat}
\icon{checkpoint}{hiCheckpoint}
\icon{child-combatant}{hiChildCombatant}
\icon{children}{hiChildren}
\icon{church}{hiChurch}
\icon{civil-military-coordination}{hiCivilMilitaryCoordination}
\icon{clinic}{hiClinic}
\icon{clothing}{hiClothing}
\icon{cold-wave}{hiColdWave}
\icon{communal-latrine}{hiCommunalLatrine}
\icon{community-building}{hiCommunityBuilding}
\icon{community-engagement}{hiCommunityEngagement}
\icon{computer}{hiComputer}
\icon{conflict}{hiConflict}
\icon{coordinated-assessement}{hiCoordinatedAssessement}
\icon{coordination}{hiCoordination}
\icon{copy}{hiCopy}
\icon{cyclone}{hiCyclone}
\icon{damaged-affected}{hiDamagedAffected}
\icon{dangerous-area}{hiDangerousArea}
\icon{data}{hiData}
\icon{dead}{hiDead}
\icon{debris-management}{hiDebrisManagement}
\icon{deployment}{hiDeployment}
\icon{destroyed}{hiDestroyed}
\icon{detergent}{hiDetergent}
\icon{diplomatic-mission}{hiDiplomaticMission}
\icon{distribution-site}{hiDistributionSite}
\icon{document}{hiDocument}
\icon{download}{hiDownload}
\icon{drought}{hiDrought}
\icon{drowned}{hiDrowned}
\icon{e-mail}{hiEMail}
\icon{early-recovery}{hiEarlyRecovery}
\icon{earthmound}{hiEarthmound}
\icon{earthquake}{hiEarthquake}
\icon{education}{hiEducation}
\icon{elderly}{hiElderly}
\icon{emergency-telecommunications}{hiEmergencyTelecommunications}
\icon{environment}{hiEnvironment}
\icon{epidemic}{hiEpidemic}
\icon{exit-cancel}{hiExitCancel}
\icon{favourite}{hiFavourite}
\icon{fax}{hiFax}
\icon{ferry}{hiFerry}
\icon{film}{hiFilm}
\icon{financing}{hiFinancing}
\icon{fire}{hiFire}
\icon{fishery}{hiFishery}
\icon{flash-flood}{hiFlashFlood}
\icon{flood}{hiFlood}
\icon{flour}{hiFlour}
\icon{folder}{hiFolder}
\icon{food-security}{hiFoodSecurity}
\icon{food-warehouse}{hiFoodWarehouse}
\icon{food}{hiFood}
\icon{forced-entry}{hiForcedEntry}
\icon{forced-recruitment}{hiForcedRecruitment}
\icon{fund}{hiFund}
\icon{gap-analysis}{hiGapAnalysis}
\icon{gas-station}{hiGasStation}
\icon{go}{hiGo}
\icon{government-office}{hiGovernmentOffice}
\icon{group}{hiGroup}
\icon{harassment-intimidation}{hiHarassmentIntimidation}
\icon{health-facility}{hiHealthFacility}
\icon{health-post}{hiHealthPost}
\icon{health}{hiHealth}
\icon{heatwave}{hiHeatwave}
\icon{heavy-rain}{hiHeavyRain}
\icon{helicopter}{hiHelicopter}
\icon{helipad}{hiHelipad}
\icon{help}{hiHelp}
\icon{hidden}{hiHidden}
\icon{hindu-temple}{hiHinduTemple}
\icon{hospital}{hiHospital}
\icon{hotel}{hiHotel}
\icon{house-affected}{hiHouseAffected}
\icon{house-burned}{hiHouseBurned}
\icon{house-destroyed}{hiHouseDestroyed}
\icon{house-not-affected}{hiHouseNotAffected}
\icon{house}{hiHouse}
\icon{humanitarian-access}{hiHumanitarianAccess}
\icon{humanitarian-programme-cycle}{hiHumanitarianProgrammeCycle}
\icon{idp-refugee-camp}{hiIdpRefugeeCamp}
\icon{infant-formula}{hiInfantFormula}
\icon{infant}{hiInfant}
\icon{information-management}{hiInformationManagement}
\icon{information-technology}{hiInformationTechnology}
\icon{infrastructure}{hiInfrastructure}
\icon{injured}{hiInjured}
\icon{innovation}{hiInnovation}
\icon{insect-infestation}{hiInsectInfestation}
\icon{internally-displaced}{hiInternallyDisplaced}
\icon{internet}{hiInternet}
\icon{kitchen-set}{hiKitchenSet}
\icon{landslide-mudslide}{hiLandslideMudslide}
\icon{laptop}{hiLaptop}
\icon{latrine-cabin}{hiLatrineCabin}
\icon{leadership}{hiLeadership}
\icon{learning}{hiLearning}
\icon{livelihood}{hiLivelihood}
\icon{livestock}{hiLivestock}
\icon{location}{hiLocation}
\icon{locust-infestation}{hiLocustInfestation}
\icon{logistics}{hiLogistics}
\icon{map}{hiMap}
\icon{mattress}{hiMattress}
\icon{medical-supply}{hiMedicalSupply}
\icon{medicine}{hiMedicine}
\icon{meeting}{hiMeeting}
\icon{menu}{hiMenu}
\icon{military-gate}{hiMilitaryGate}
\icon{mine}{hiMine}
\icon{missing}{hiMissing}
\icon{mobile-clinic}{hiMobileClinic}
\icon{mobile-phone}{hiMobilePhone}
\icon{monitor}{hiMonitor}
\icon{monitoring}{hiMonitoring}
\icon{more-options}{hiMoreOptions}
\icon{mosque}{hiMosque}
\icon{mosquito-net}{hiMosquitoNet}
\icon{multi-cluster-sector}{hiMultiClusterSector}
\icon{murder}{hiMurder}
\icon{national-army}{hiNationalArmy}
\icon{needs-assessment}{hiNeedsAssessment}
\icon{next-item}{hiNextItem}
\icon{ngo-office}{hiNgoOffice}
\icon{non-food-items}{hiNonFoodItems}
\icon{not-affected}{hiNotAffected}
\icon{notification}{hiNotification}
\icon{nutrition}{hiNutrition}
\icon{observation-tower}{hiObservationTower}
\icon{oil}{hiOil}
\icon{out-of-platform}{hiOutOfPlatform}
\icon{partnership}{hiPartnership}
\icon{pause}{hiPause}
\icon{peacekeeping-force}{hiPeacekeepingForce}
\icon{people-with-physical-impairments}{hiPeopleWithPhysicalImpairments}
\icon{permanent-camp}{hiPermanentCamp}
\icon{person-1}{hiPerson1}
\icon{person-2}{hiPerson2}
\icon{photo}{hiPhoto}
\icon{physical-closure}{hiPhysicalClosure}
\icon{plastic-sheeting}{hiPlasticSheeting}
\icon{police-station}{hiPoliceStation}
\icon{policy}{hiPolicy}
\icon{population-growth}{hiPopulationGrowth}
\icon{population-return}{hiPopulationReturn}
\icon{port-affected}{hiPortAffected}
\icon{port-destroyed}{hiPortDestroyed}
\icon{port-not-affected}{hiPortNotAffected}
\icon{port}{hiPort}
\icon{potable-water-source}{hiPotableWaterSource}
\icon{potable-water}{hiPotableWater}
\icon{pregnant}{hiPregnant}
\icon{preparedness}{hiPreparedness}
\icon{previous-item}{hiPreviousItem}
\icon{print}{hiPrint}
\icon{protection}{hiProtection}
\icon{public-information}{hiPublicInformation}
\icon{radio}{hiRadio}
\icon{rebel}{hiRebel}
\icon{reconstruction}{hiReconstruction}
\icon{refugee}{hiRefugee}
\icon{registration}{hiRegistration}
\icon{relief-goods}{hiReliefGoods}
\icon{remove-document}{hiRemoveDocument}
\icon{remove}{hiRemove}
\icon{report}{hiReport}
\icon{reporting}{hiReporting}
\icon{response}{hiResponse}
\icon{return}{hiReturn}
\icon{rice}{hiRice}
\icon{road-affected}{hiRoadAffected}
\icon{road-barrier}{hiRoadBarrier}
\icon{road-destroyed}{hiRoadDestroyed}
\icon{road-not-affected}{hiRoadNotAffected}
\icon{road}{hiRoad}
\icon{roadblock}{hiRoadblock}
\icon{robbery}{hiRobbery}
\icon{rule-of-law-and-justice}{hiRuleOfLawAndJustice}
\icon{rural-exodus}{hiRuralExodus}
\icon{rural}{hiRural}
\icon{safety-and-security}{hiSafetyAndSecurity}
\icon{salt}{hiSalt}
\icon{sanitation}{hiSanitation}
\icon{satellite-dish}{hiSatelliteDish}
\icon{save}{hiSave}
\icon{scale-down-operation}{hiScaleDownOperation}
\icon{scale-up-operation}{hiScaleUpOperation}
\icon{school-affected}{hiSchoolAffected}
\icon{school-destroyed}{hiSchoolDestroyed}
\icon{school-not-affected}{hiSchoolNotAffected}
\icon{school}{hiSchool}
\icon{search}{hiSearch}
\icon{see}{hiSee}
\icon{selected}{hiSelected}
\icon{services-and-tools}{hiServicesAndTools}
\icon{settings}{hiSettings}
\icon{sexual-violence}{hiSexualViolence}
\icon{share}{hiShare}
\icon{shelter}{hiShelter}
\icon{ship}{hiShip}
\icon{shower}{hiShower}
\icon{smartphone}{hiSmartphone}
\icon{snow-avalanche}{hiSnowAvalanche}
\icon{snowfall}{hiSnowfall}
\icon{soap}{hiSoap}
\icon{solid-waste}{hiSolidWaste}
\icon{spontaneous-site}{hiSpontaneousSite}
\icon{spring-water}{hiSpringWater}
\icon{staff-management}{hiStaffManagement}
\icon{stop}{hiStop}
\icon{storm-surge}{hiStormSurge}
\icon{storm}{hiStorm}
\icon{stove}{hiStove}
\icon{submersible-pump}{hiSubmersiblePump}
\icon{sugar}{hiSugar}
\icon{table}{hiTable}
\icon{tarpaulin}{hiTarpaulin}
\icon{technological-disaster}{hiTechnologicalDisaster}
\icon{temporary-camp}{hiTemporaryCamp}
\icon{tent}{hiTent}
\icon{toilet}{hiToilet}
\icon{tornado}{hiTornado}
\icon{trade-and-market}{hiTradeAndMarket}
\icon{train}{hiTrain}
\icon{transition-site}{hiTransitionSite}
\icon{trending}{hiTrending}
\icon{truck}{hiTruck}
\icon{tsunami}{hiTsunami}
\icon{tunnel}{hiTunnel}
\icon{un-compound-office}{hiUnCompoundOffice}
\icon{un-vehicle}{hiUnVehicle}
\icon{university}{hiUniversity}
\icon{upload}{hiUpload}
\icon{urban-rural}{hiUrbanRural}
\icon{urban}{hiUrban}
\icon{user}{hiUser}
\icon{users}{hiUsers}
\icon{vaccine}{hiVaccine}
\icon{video}{hiVideo}
\icon{violent-wind}{hiViolentWind}
\icon{volcano}{hiVolcano}
\icon{walkie-talkie}{hiWalkieTalkie}
\icon{warning-error}{hiWarningError}
\icon{water-sanitation-and-hygiene}{hiWaterSanitationAndHygiene}
\icon{water-source}{hiWaterSource}
\icon{water-trucking}{hiWaterTrucking}
\icon{zip-compressed}{hiZipCompressed}
\end{showcase}

\PrintChanges
% \PrintIndex
\end{document}
