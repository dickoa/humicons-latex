# humicons

The *humicons* package provides access in (La)TeX to 295 high quality humanitarian icons included in the free *OCHA Huminatarian icon v2.0*.
 
This package is a fork of the [academicons](https://github.com/diogo-fernan/academicons) latex packages and was influenced by the [fontawesome](	https://github.com/xdanaux/fontawe­some-la­tex) latex package. 
 
humicons (La)TeX package\
Version: 2.0.0\
License: LaTeX Project Public License, version 1.3c
